package com.bblackbelt.gituhubusers.di

import com.bblackbelt.gituhubusers.repository.details.IUserDetailsRepository
import com.bblackbelt.gituhubusers.repository.details.UserDetailsRepository
import com.bblackbelt.gituhubusers.repository.users.IUserDataRepository
import com.bblackbelt.gituhubusers.repository.users.UsersDataRepository
import dagger.Binds
import dagger.Module

@Module
abstract class BindsModule {

    @Binds
    abstract fun bindsUserDataRepository(userData: UsersDataRepository): IUserDataRepository

    @Binds
    abstract fun bindsUserDetailsRepository(userData: UserDetailsRepository): IUserDetailsRepository
}