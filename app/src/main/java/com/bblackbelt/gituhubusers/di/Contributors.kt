package com.bblackbelt.gituhubusers.di

import com.bblackbelt.gituhubusers.api.model.UserDetails
import com.bblackbelt.gituhubusers.view.details.UserDetailsActivity
import com.bblackbelt.gituhubusers.view.users.UsersActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface Contributors {

    @ContributesAndroidInjector
    fun usersActivity() : UsersActivity

    @ContributesAndroidInjector
    fun userDetailsActivity() : UserDetailsActivity
}