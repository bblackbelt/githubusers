package com.bblackbelt.gituhubusers.view.users.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bblackbelt.gituhubusers.R
import com.bblackbelt.gituhubusers.api.model.User
import com.bblackbelt.gituhubusers.utils.setGitHubAvatar
import com.bblackbelt.gituhubusers.view.details.UserDetailsActivity

class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val username = view.findViewById<TextView>(R.id.username)
    private val avatar = view.findViewById<ImageView>(R.id.user_image)

    private var user: User? = null

    init {
        view.setOnClickListener({
            val intent = Intent(it.context, UserDetailsActivity::class.java)
            intent.putExtra(UserDetailsActivity.ID_KEY, user?.login)
            it.context.startActivity(intent)
        })
    }

    fun bind(item: User?) {
        user = item
        username.text = item?.login
        avatar.setGitHubAvatar(item?.avatarUrl)
    }
}