package com.bblackbelt.gituhubusers.view.users.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bblackbelt.gituhubusers.repository.users.IUserDataRepository
import javax.inject.Inject


class UsersViewModel(private val usersDataRepository: IUserDataRepository) : ViewModel() {

    private val pageSize = MutableLiveData<Int>()
    private val repoResult =
            Transformations.map(pageSize, { usersDataRepository.getUsers(it) })

    init {
        pageSize.value = 50
    }

    val users = Transformations.switchMap(repoResult, { it.pagedList })!!
    val networkState = Transformations.switchMap(repoResult, { it.networkState })!!

    class Factory @Inject constructor(val usersDataRepository: IUserDataRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = UsersViewModel(usersDataRepository) as T
    }

    fun retry() {
        repoResult?.value?.retry?.invoke()
    }
}