package com.bblackbelt.gituhubusers.view.users.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.bblackbelt.gituhubusers.R
import com.bblackbelt.gituhubusers.repository.users.NetworkState

class NetworkStateViewHolder(view: View, private val retryCallback: () -> Unit) : RecyclerView.ViewHolder(view) {

    private val progressBar = view.findViewById<ProgressBar>(R.id.progress_bar)
    private val retry = view.findViewById<Button>(R.id.retry_button)
    private val errorMsg = view.findViewById<TextView>(R.id.error_msg)

    init {
        retry.setOnClickListener {
            retryCallback()
        }
    }

    fun bind(state: NetworkState?) {
        progressBar.toVisibility(state == NetworkState.LOADING)
        retry.toVisibility(state == NetworkState.FAILED)
        errorMsg.toVisibility(state == NetworkState.FAILED)
    }
}

fun View.toVisibility(visible: Boolean) {
    visibility = when (visible) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}