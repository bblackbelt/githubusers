package com.bblackbelt.gituhubusers.view.users

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bblackbelt.gituhubusers.R
import com.bblackbelt.gituhubusers.api.model.User
import com.bblackbelt.gituhubusers.view.users.adapter.UsersAdapter
import com.bblackbelt.gituhubusers.view.users.viewmodel.UsersViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class UsersActivity : AppCompatActivity() {

    private val mItemDecoration by lazy {
        val margin: Int = resources.getDimension(R.dimen.margin_4).toInt()
        val lateralMargin: Int = resources.getDimension(R.dimen.margin_16).toInt()
        object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
                outRect?.bottom = margin
                outRect?.top = margin
                outRect?.left = lateralMargin
                outRect?.right = lateralMargin
            }
        }
    }

    @Inject
    lateinit var mFactory: UsersViewModel.Factory

    private val mViewModel: UsersViewModel by lazy {
        ViewModelProviders.of(this, mFactory)[UsersViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAdapter()
    }

    private fun initAdapter() {
        val adapter = UsersAdapter(
                { mViewModel.retry() }
        )
        list.adapter = adapter
        mViewModel.users.observe(this, Observer<PagedList<User>> {
            adapter.submitList(it)
        })
        mViewModel.networkState.observe(this, Observer {
            adapter.setNetworkState(it)
        })
        list.addItemDecoration(mItemDecoration)
    }
}