package com.bblackbelt.gituhubusers.view.details.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.view.View
import com.bblackbelt.gituhubusers.api.model.UserDetails
import com.bblackbelt.gituhubusers.repository.details.IUserDetailsRepository
import com.bblackbelt.gituhubusers.repository.users.IUserDataRepository
import com.bblackbelt.gituhubusers.repository.users.NetworkState
import com.bblackbelt.gituhubusers.view.users.viewmodel.UsersViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import javax.inject.Inject

class UserDetailsViewModel(private val userDetailsRepository: IUserDetailsRepository) : ViewModel() {

    var mDetailsDisposable = Disposables.disposed()

    val userLiveData = MutableLiveData<UserDetails>()

    val networkState = MutableLiveData<NetworkState>()

    var userId: String? = null
        set(value) {
            field = value
            if (value.isNullOrEmpty()) {
                return
            }
            networkState.postValue(NetworkState.LOADING)
            mDetailsDisposable =
                    userDetailsRepository.getUserDetails(value!!)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                userLiveData.value = it
                                networkState.postValue(NetworkState.LOADED)
                            }, {})
        }

    override fun onCleared() {
        super.onCleared()
        mDetailsDisposable.dispose()
    }

    class Factory @Inject constructor(private val usersDetailsRepository: IUserDetailsRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = UserDetailsViewModel(usersDetailsRepository) as T
    }
}