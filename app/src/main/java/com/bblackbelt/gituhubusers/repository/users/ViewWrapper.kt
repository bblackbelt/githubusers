package com.bblackbelt.gituhubusers.repository.users

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class ViewWrapper<T>(
        val pagedList: LiveData<PagedList<T>>,
        val networkState: LiveData<NetworkState>,
        val retry: () -> Unit)