package com.bblackbelt.gituhubusers.repository.details

import com.bblackbelt.gituhubusers.api.model.UserDetails
import io.reactivex.Observable


interface IUserDetailsRepository {
    fun getUserDetails(user: String): Observable<UserDetails>
}