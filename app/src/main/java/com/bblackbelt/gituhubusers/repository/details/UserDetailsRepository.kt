package com.bblackbelt.gituhubusers.repository.details


import com.bblackbelt.gituhubusers.api.GitHubService
import com.bblackbelt.gituhubusers.api.model.UserDetails
import io.reactivex.Observable
import javax.inject.Inject

class UserDetailsRepository @Inject constructor(private val gitHubService: GitHubService) : IUserDetailsRepository {

    override fun getUserDetails(user: String): Observable<UserDetails> {
        return gitHubService.getUserDetails(user)
    }
}