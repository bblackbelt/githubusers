package com.bblackbelt.gituhubusers.repository.users

enum class NetworkState {
    LOADED,
    LOADING,
    FAILED
}

