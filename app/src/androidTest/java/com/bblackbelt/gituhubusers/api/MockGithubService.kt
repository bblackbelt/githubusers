package com.bblackbelt.gituhubusers.api

import com.bblackbelt.gituhubusers.api.model.User
import com.bblackbelt.gituhubusers.api.model.UserDetails
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls
import java.io.IOException
import java.util.*

class MockGithubService(private val delegate: BehaviorDelegate<GitHubService>) : GitHubService {

    var failureMsg: String? = null

    val users: MutableMap<String, User> = mutableMapOf()

    override fun getUsers(page: Int, pageSize: Int): Observable<List<User>> {
        failureMsg?.let {
            return delegate.returning(Calls.failure<Any>(IOException())).getUsers()
        }

        for (i in page until page + pageSize) {
            val user = generateUser(i)
            users[user.login!!] = user
        }

        val response = Response.success(users.values.toList())
        return delegate.returning(Calls.response(response)).getUsers(page, pageSize)
    }

    override fun getUserDetails(id: String): Observable<UserDetails> {
        failureMsg?.let {
            return@let Calls.failure<IOException>(IOException(it))
        }
        val theId: Int = id.toIntOrNull() ?: 0
        val userExists = Random().nextBoolean() && users[id] != null
        return when (userExists) {
            true -> delegate.returning(Calls.response(Response.success(generateUsers(theId)))).getUserDetails(id)
            false -> {
                val json = JSONObject()
                val error = Response.error<Any>(404, ResponseBody
                        .create(MediaType.parse("application/json"), json.toString()))
                delegate.returning(Calls.response(error)).getUserDetails(id)
            }
        }
    }
}

fun generateUser(id: Int = 0): User =
        User(
                "http://gisturl",
                "http//repourl",
                "http://followingUrl",
                "http://starredUrl",
                "user_login$id",
                "http://followersUrl",
                "user",
                "http://url",
                "http://subscriptionsUrl",
                "http://receivedEventsUrl",
                "http://avatarUrl",
                "http://eventsUrl",
                "http://htmlUrl",
                false,
                id,
                "gravatar$id",
                "http://organizationsUrl")

fun generateUsers(page: Int = 0, num: Int = 25): List<User> {
    val items: MutableList<User> = mutableListOf()
    for (i in page until page + num) {
        items.add(generateUser(i))
    }
    return items.toList()
}
