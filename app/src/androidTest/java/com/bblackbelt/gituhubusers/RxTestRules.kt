package com.bblackbelt.gituhubusers

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement


class RxTestRules : TestRule {

    private var mScheduler: Scheduler = Schedulers.trampoline()

    fun resetSchedulers() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

    override fun apply(base: Statement, description: Description): Statement {

        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {

                RxJavaPlugins.setInitIoSchedulerHandler { mScheduler }
                RxJavaPlugins.setIoSchedulerHandler { mScheduler }

                RxJavaPlugins.setInitComputationSchedulerHandler { mScheduler }
                RxJavaPlugins.setComputationSchedulerHandler { mScheduler }

                RxJavaPlugins.setInitNewThreadSchedulerHandler { mScheduler }
                RxJavaPlugins.setNewThreadSchedulerHandler { mScheduler }

                RxJavaPlugins.setInitSingleSchedulerHandler { mScheduler }
                RxJavaPlugins.setSingleSchedulerHandler { mScheduler }

                RxAndroidPlugins.setInitMainThreadSchedulerHandler { mScheduler }

                try {
                    base.evaluate()
                } finally {
                    resetSchedulers()
                }
            }
        }
    }
}