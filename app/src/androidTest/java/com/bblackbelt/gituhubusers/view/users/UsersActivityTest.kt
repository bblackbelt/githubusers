package com.bblackbelt.gituhubusers.view.users

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.bblackbelt.gituhubusers.GitHubTestApp
import com.bblackbelt.gituhubusers.R
import com.bblackbelt.gituhubusers.RxTestRules
import com.bblackbelt.gituhubusers.api.GitHubService
import com.bblackbelt.gituhubusers.api.MockGithubService
import com.bblackbelt.gituhubusers.view.details.UserDetailsActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class UsersActivityTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    @get:Rule
    val rxR = RxTestRules()

    @JvmField
    @Rule
    var usersActivity: ActivityTestRule<UsersActivity> =
            ActivityTestRule(UsersActivity::class.java, false, false)

    @Inject
    lateinit var githubService: GitHubService

    @Before
    fun setup() {
        GitHubTestApp.instance.component.inject(this)
    }

    @Test
    fun assert_results_load() {
        usersActivity.launchActivity(Intent())
        val recyclerView = usersActivity.activity.findViewById<RecyclerView>(R.id.list)
        Assert.assertTrue(recyclerView != null)
        Assert.assertTrue(recyclerView.adapter != null)

        Espresso.onView(withId(R.id.list))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // loading
        // results
        // loaded
        waitForAdapterChange(usersActivity.activity.findViewById(R.id.list), 3)
        val count: Int = recyclerView.adapter.itemCount
        Assert.assertTrue(count == 50)
    }

    @Test
    fun assert_results_item_click() {
        usersActivity.launchActivity(Intent())
        val recyclerView = usersActivity.activity.findViewById<RecyclerView>(R.id.list)
        Assert.assertTrue(recyclerView != null)
        Assert.assertTrue(recyclerView.adapter != null)

        Espresso.onView(withId(R.id.list))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // loading
        // results
        // loaded
        waitForAdapterChange(usersActivity.activity.findViewById(R.id.list), 3)
        val activityMonitor = InstrumentationRegistry.getInstrumentation()
                .addMonitor(UserDetailsActivity::class.java.name, null, true)

        Espresso.onView(ViewMatchers.withId(R.id.list))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))

        Assert.assertTrue(activityMonitor.hits == 1)

        InstrumentationRegistry.getInstrumentation().removeMonitor(activityMonitor)
    }

    @Test
    fun assert_results_load_fail() {

        (githubService as MockGithubService).failureMsg = "xx"
        usersActivity.launchActivity(Intent())

        val recyclerView = usersActivity.activity.findViewById<RecyclerView>(R.id.list)
        Assert.assertTrue(recyclerView != null)
        Assert.assertTrue(recyclerView.adapter != null)

        waitForAdapterChange(recyclerView, 2)
        Assert.assertTrue(recyclerView.adapter.itemCount == 1)
        Assert.assertTrue(recyclerView.adapter.getItemViewType(0) == R.layout.network_state_item)
    }

    @Test
    fun assert_results_retry() {

        (githubService as MockGithubService).failureMsg = "xx"
        usersActivity.launchActivity(Intent())

        val recyclerView = usersActivity.activity.findViewById<RecyclerView>(R.id.list)
        Assert.assertTrue(recyclerView != null)
        Assert.assertTrue(recyclerView.adapter != null)

        (githubService as MockGithubService).failureMsg = null

        waitForAdapterChange(recyclerView, 2)
        Assert.assertTrue(recyclerView.adapter.itemCount == 1)
        Assert.assertTrue(recyclerView.adapter.getItemViewType(0) == R.layout.network_state_item)

        Espresso.onView(withId(R.id.retry_button)).perform(ViewActions.click())

        waitForAdapterChange(recyclerView, 3)
        Assert.assertTrue(recyclerView.adapter.itemCount == 50)
    }

    private fun waitForAdapterChange(recyclerView: RecyclerView, eventsNum: Int = 2) {
        val latch = CountDownLatch(eventsNum)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            recyclerView.adapter.registerAdapterDataObserver(
                    object : RecyclerView.AdapterDataObserver() {
                        override fun onChanged() {
                            latch.countDown()
                        }

                        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                            latch.countDown()
                        }
                    })
        }
        if (recyclerView.adapter.itemCount > 0) {
            return
        }
        MatcherAssert.assertThat(latch.await(10, TimeUnit.SECONDS), CoreMatchers.`is`(true))
    }
}