package com.bblackbelt.gituhubusers.view.users.di

import com.bblackbelt.gituhubusers.api.GitHubService
import com.bblackbelt.gituhubusers.api.MockGithubService
import com.bblackbelt.gituhubusers.repository.users.UsersPageKeyedDataSource
import com.bblackbelt.gituhubusers.view.users.UsersActivity
import dagger.Module
import dagger.Provides

@Module
class UsersModule {

    @Provides
    fun provide_test(usersActivity: UsersActivity, service: GitHubService): UsersPageKeyedDataSource {
        if (usersActivity.intent.getBooleanExtra("fail", true)) {
            (service as MockGithubService).failureMsg = "xx"
        }
        return UsersPageKeyedDataSource(service)
    }

}