package com.bblackbelt.gituhubusers.di

import com.bblackbelt.gituhubusers.api.GitHubService
import com.bblackbelt.gituhubusers.api.MockGithubService
import com.bblackbelt.gituhubusers.repository.users.IUserDataRepository
import com.bblackbelt.gituhubusers.view.users.UsersActivity
import com.bblackbelt.gituhubusers.view.users.viewmodel.UsersViewModel
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import javax.inject.Singleton

@Module
class NetworkModuleTest {

    @Singleton
    @Provides
    fun provideGitHubApiService(): GitHubService {
        val retrofit = Retrofit.Builder().baseUrl("http://test.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val behavior = NetworkBehavior.create()
        behavior.setFailurePercent(0)

        val mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build()

        val delegate = mockRetrofit.create(GitHubService::class.java)
        return MockGithubService(delegate)
    }
}