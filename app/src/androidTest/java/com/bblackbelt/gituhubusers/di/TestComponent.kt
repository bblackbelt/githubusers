package com.bblackbelt.gituhubusers.di

import com.bblackbelt.gituhubusers.GitHubApp
import com.bblackbelt.gituhubusers.GitHubTestApp
import com.bblackbelt.gituhubusers.view.details.UserDetailsActivity
import com.bblackbelt.gituhubusers.view.users.UsersActivityTest
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),
    (NetworkModuleTest::class), (Contributors::class), (BindsModule::class)])
interface TestComponent {

    fun inject(act : UsersActivityTest)

    fun inject(app: GitHubTestApp)
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: GitHubTestApp): Builder
        fun build(): TestComponent
    }
}