package com.bblackbelt.gituhubusers

import com.bblackbelt.gituhubusers.di.DaggerTestComponent
import com.bblackbelt.gituhubusers.di.TestComponent

class GitHubTestApp : GitHubApp() {

    companion object {
        lateinit var instance: GitHubTestApp
            private set
    }

    val component: TestComponent by lazy {
        DaggerTestComponent
                .builder()
                .application(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        component.inject(this)
    }
}